// Mảng ban đầu
var myArr = [];
var newArr = [];


function addNumber() {

    var numberEl = document.getElementById('txt-number');
    var resultEl = document.getElementById('result');
    // get data
    var myNum = numberEl.value * 1;

    // validate data
    if (numberEl.value == '') {
        alert("Vui lòng nhập số");
        return;
    }

    //add data
    myArr.push(myNum);
    // console.log('myArr: ', myArr);

    //show arr
    resultEl.innerHTML = myArr;
    //set to default
    numberEl.value = '';
}
function handleKeyPress(event, myFunction) {
    if (event.keyCode == 13) {
        // addNumber();
        myFunction();
        event.preventDefault();
    }
}
// Bai tap 1
function tinhTong() {
    var result1 = document.getElementById('result1');

    var sum = 0;

    myArr.forEach((item) => {
        if (item > 0) {
            sum += item;
        }
    });


    result1.innerHTML = `Tổng số dương: ${sum}`;

}

// Bai tap 2

function demSoDuong() {

    var result2 = document.getElementById('result2');
    // console.log('result2: ', result2);
    let sum = 0;
    myArr.forEach((item) => {
        if (item > 0) {
            sum += 1;
        }
    });
    // console.log(sum);
    result2.innerHTML = sum;



}

// Bai tap 3
function timSoNhoNhat() {
    var result3 = document.getElementById("result3");

    var min = myArr[0];

    myArr.forEach((item) => {
        if (min > item) {
            min = item;
        }
    })
    // console.log(min);
    result3.innerHTML = min;
}
// Bai tap 4
function timSoDuongNhoNhat() {
    var result4 = document.getElementById('result4');

    var positiveArr = [];
    myArr.forEach((item) => {
        if (item > 0) {
            positiveArr.push(item);
        }
    })
    var min = positiveArr[0];
    positiveArr.forEach((item) => {
        if (item > 0) {
            if (min > item) {
                min = item;
            }
        }
    })
    result4.innerHTML = min;

}

// Bai tap 5
function timSoChanCuoiCung() {
    var result5 = document.getElementById("result5");
    // console.log('result5: ', result5);

    var myNum = -1;

    myArr.forEach((item) => {
        if (item % 2 == 0) {
            myNum = item;
        }

    })
    result5.innerHTML = `Số chẵn cuối cùng là: ${myNum}`

}
//Bài tập 6
function doiCho() {
    var viTri1 = document.getElementById("txt-vi-tri-1").value * 1;
    var viTri2 = document.getElementById("txt-vi-tri-2").value * 1;
    var result6 = document.getElementById('result6');

    var copyArr = [...myArr];


    var temp = copyArr[viTri1];

    copyArr[viTri1] = copyArr[viTri2];
    copyArr[viTri2] = temp;

    result6.innerHTML = `Mảng sau khi đổi là: ${copyArr}`;
}

//Bài tập 7:
function sapXepTangDan() {
    var result7 = document.getElementById("result7");

    var arrCopy = [...myArr];
    for (var i = 0; i < myArr.length; i++) {
        for (var j = 0; j < myArr.length - 1; j++) {
            if (arrCopy[j] > arrCopy[j + 1]) {
                var temp = arrCopy[j];
                arrCopy[j] = arrCopy[j + 1];
                arrCopy[j + 1] = temp;
            }
        }
    }
    // console.log(myArr);
    result7.innerHTML = `Mảng sau khi sắp xếp: ${arrCopy}`;

}

//Bài tập 8:
function timSoNguyenToDauTien() {
    var result8 = document.getElementById("result8");

    var copyArr = [...myArr];

    // var soNguyenToArr = [];
    //tim số nguyên tố: snt là số chỉ chia hết cho 1 và chính nó
    var snt = 0;

    for (var i = 0; i < copyArr.length; i++) {
        if (laSoNguyenTo(copyArr[i])) {
            // soNguyenToArr.push(item);
            snt = copyArr[i]
            result8.innerHTML = `Số nguyên tố đầu tiên là:  ${snt}`
            // console.log("Chay vao day");
            break;
        }

    }
    if (!snt) {
        snt = -1;
        result8.innerHTML = `Số nguyên tố đầu tiên là:  ${snt}`
    }



}

function laSoNguyenTo(n) {
    if (n < 2) {
        return false;
    }

    // Neu n = 2 la SNT
    if (n === 2) {
        return true;
    }

    // Neu n la so chan thi ko phai la SNT
    if (n % 2 === 0) {
        return false;
    }

    // Lap qua cac so le
    for (var i = 3; i < (n - 1); i += 2) {
        if (n % i === 0) {
            return false;

        }
    }

    return true;

}
// Bài tập 9:
function themMangMoi() {
    var newNum = document.getElementById("txt-new-num");
    var infoEl = document.getElementById("info");
    // get data

    var number = newNum.value * 1;

    // add to new arr

    newArr.push(number);
    // console.log('newArr: ', newArr);
    infoEl.innerHTML = newArr;
    newNum.value = '';

}

function demSoNguyen() {
    var result9 = document.getElementById("result9");

    var sum = 0;
    newArr.forEach((item) => {
        if (Number.isInteger(item)) {
            sum += 1;
        }

    })

    // console.log('sum: ', sum);
    result9.innerHTML = `Tổng số nguyên là: ${sum}`;
}

// bài tập 10
function soSanhAmDuong() {
    var result10 = document.getElementById('result10');
    // console.log('result10: ', result10);

    var tongAm = 0;
    var tongDuong = 0;
    myArr.forEach((item) => {

        if (item > 0) {
            tongDuong += 1;
        } else if (item < 0) {
            tongAm += 1;
        }

    })

    if (tongDuong > tongAm) {
        result10.innerHTML = "Tổng dương > tổng âm";
    } else if (tongDuong == tongAm) {
        result10.innerHTML = "Tổng dương = tổng âm";

    } else {
        result10.innerHTML = "Tổng dương < tổng âm";

    }
}












